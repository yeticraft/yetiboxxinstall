angular.module('yetiboxui', ['angular-meteor', 'ui.router']);
MainApp  = angular.module('yetiboxui');
angular.module('yetiboxui').config([ '$interpolateProvider', '$sceDelegateProvider',
  function ($interpolateProvider, $sceDelegateProvider) {

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
    $sceDelegateProvider.resourceUrlWhitelist([
      // Allow same origin resource loads.
      'self',
      // Allow loading from our assets domain.  Notice the difference between * and **.
      '*'
    ]);
  }
]);

function onReady() {
  angular.bootstrap(document, ['yetiboxui']);
}

if (Meteor.isCordova){
  angular.element(document).on("deviceready", onReady);
}else{
  angular.element(document).ready(onReady);
}

//Meteor subscirbe
Meteor.subscribe("userfiles");
