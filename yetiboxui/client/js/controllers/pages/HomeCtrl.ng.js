//Home page controller
MainApp.controller('HomeCtrl', ['$scope', '$meteor', '$rootScope', function($scope, $meteor, $rootScope){
    $scope.callscript = function(switchvar){
      switch (switchvar) {
          case 'btnone':
            //Statements executed when the result of expression matches btntwo
            $meteor.call('clickbuttone').then(
                // OnSuccess
                function(ReturnDataFromServer){
                  console.log(ReturnDataFromServer);
                },
                // OnFailure
                function(ReturnErrorFromServer){
                  console.log(ReturnErrorFromServer);
                });
            break;
          case 'btntwo':
            //Statements executed when the result of expression matches btntwo
            $meteor.call('clickbutttwo').then(
                // OnSuccess
                function(ReturnDataFromServer){
                  console.log(ReturnDataFromServer);
                },
                // OnFailure
                function(ReturnErrorFromServer){
                  console.log(ReturnErrorFromServer);
                });
            break;
          case 'stop_spigot':
            //Statements executed when the result of expression matches btnthree
            $meteor.call('stop_spigot_method').then(
                // OnSuccess
                function(ReturnDataFromServer){
                  console.log(ReturnDataFromServer);
                  $scope.stop_spigot_info = ReturnDataFromServer;

                },
                // OnFailure
                function(ReturnErrorFromServer){
                  console.log(ReturnErrorFromServer);
                });
            break;
          case 'run_spigot':
              //Statements executed when the result of expression matches btnfour
              $meteor.call('run_spigot_method').then(
                  // OnSuccess
                  function(ReturnDataFromServer){
                    console.log(ReturnDataFromServer);
                    $scope.run_spigot_info = ReturnDataFromServer;
                  },
                  // OnFailure
                  function(ReturnErrorFromServer){
                    console.log(ReturnErrorFromServer);
                  });
            break;
          default:
            //Statements executed when none of the values match the value of the expression
            break;
        }
    };

}]);
