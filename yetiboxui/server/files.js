/**
 * Files
 * @docs https://github.com/CollectionFS/Meteor-CollectionFS/tree/devel/packages/filesystem
 */
UserFiles.allow({
  download: function () {
    return true;
  },
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});
Meteor.publish("userfiles", function () {
    return UserFiles.find();
});
