var Future    = Meteor.npmRequire('fibers/future');
var sys       = Meteor.npmRequire('sys');
var exec      = Meteor.npmRequire('child_process').exec;
var path      = Npm.require('path');
var fs        = Npm.require('fs');
var base      = path.resolve('.');

Meteor.methods({
    clickbuttone: function(argsfromclient){
        var mc =  Meteor.npmRequire('minecraft-protocol');
        var server = mc.createServer({
          'online-mode': true,   // optional
          encryption: true,      // optional
          host: '0.0.0.0',       // optional
          port: 25565,           // optional
        });
        console.log(server);
        return server;

    },
    clickbutttwo: function(argsfromclient){

        var myfuture  = new Future();
        __dirname = path.resolve(path.dirname());

        var out = exec("ls -la ",
          function(error, stdout, stderr) {
            myfuture.return(stdout);
          }
        );
        return myfuture.wait();
    },
    stop_spigot_method: function(argsfromclient){


        var myfuture  = new Future();
        __dirname = path.resolve(path.dirname());

        var out = exec("/home/pi/YetiBoxX/system/init/minecraft stop ",
          function(error, stdout, stderr) {
            console.log(error);
            console.log(stderr);
            myfuture.return(stdout);
          }
        );
        return myfuture.wait();
    },
    run_spigot_method: function(argsfromclient){

        var myfuture  = new Future();
        __dirname = path.resolve(path.dirname());

        var out = exec("/home/pi/YetiBoxX/system/init/minecraft start ",
          function(error, stdout, stderr) {
            console.log(error);
            console.log(stderr);
            myfuture.return(stdout);
          }
        );
        return myfuture.wait();
    },
});
