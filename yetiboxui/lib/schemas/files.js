/**
 * Files
 * @docs https://github.com/CollectionFS/Meteor-CollectionFS/tree/devel/packages/filesystem
 */
var createThumb = function(fileObj, readStream, writeStream) {
  // Transform the image into a 10x10px thumbnail
  gm(readStream, fileObj.name()).resize('60', '60').stream().pipe(writeStream);
};
var ThumbImageStore = new FS.Store.FileSystem("thumbs",{
  //path: "../../../streamimgs/thumbs", //optional, default is "/cfs/files" path within app container
  transformWrite: createThumb,
});

/**
 * User images
 */
var UserFilesStore = new FS.Store.FileSystem("useravatar", {
 maxTries: 1 //optional, default 5
});
UserFiles = new FS.Collection("userfiles", {
  stores: [
    UserFilesStore,
    ThumbImageStore,
  ],
  filter: {
    maxSize: 1048576 * 4,
    allow: {
      contentTypes: ['image/*'],
      extensions: ['jpg', 'png', 'gif']
    }
  }
});
