/*
 * User data and profile information
 */
var Schemas = {};
Schemas.UserCountry = new SimpleSchema({
    name: {
        type: String
    },
    code: {
        type: String,
        regEx: /^[A-Z]{2}$/
    }
});
Schemas.UserProfile = new SimpleSchema({
    name: {
        type: String,
        optional: true
    },
    lastName: {
        type: String,
        optional: true
    },
    organization : {
        type: String,
        optional: true
    },
    website: {
        type: String,
        defaultValue: '',
        optional: true
    },
    facebook: {
        type: String,
        defaultValue: '',
        optional: true
    },
    twitter: {
        type: String,
        defaultValue: '',
        optional: true
    },
    gplus: {
        type: String,
        defaultValue: '',
        optional: true
    },
    sex: {
        type: String,
        optional: true,
        defaultValue: 'Male',
        autoform: {
          type: 'select',
          options: function(){
            return  [{label: 'Male', value: 'Male'}, {label: 'Female', value: 'Female'}];
          }
        }
    },
    bio: {
        type: String,
        optional: true,
        autoform: {
            afFieldInput: {
                type: 'textangular',
            }
        }
    },
    city: {
      type: String,
      optional: true,
    },
    country: {
        type: String,
        optional: true,
        autoform: {
          type: 'select',
          options: function(){
            //returns the streams and allows a chat room to be assigned to one :D
            //returns the users list and allows a one to be assigned as the owner
            var countries_list = Countries.find().fetch()[0];
            return _.map(countries_list.countries, function (value, key) {
              return {label: value.name, value: value.code};
            });
          }
        }
    }
});
Schemas.User = new SimpleSchema({
    username: {
        type: String,
        label: 'Username',
        optional: true,
    },
    emails: {
        type: [Object],
        // this must be optional if you also use other login services like facebook,
        // but if you use only accounts-password, then it can be required
        optional: true
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean,
        autoform: {
            omit: true
        }
    },
    profile: {
        type: Schemas.UserProfile,
        optional: true
    },
    services: {
        type: Object,
        label: 'Services',
        optional: true,
        blackbox: true,
        autoform: {
            omit: true
        }
    },
    profileheader: {
      type: String,
      label: 'Profile header',
      optional: true,
      autoform: {
        afFieldInput: {
          type: "cfs-file",
          collection: "userfiles"
        }
      }
    },
    profileavatar: {
      type: String,
      label: 'Avatar',
      optional: true,
      autoform: {
        afFieldInput: {
          type: "cfs-file",
          collection: "userfiles"
        }
      }
    },
    // Option 2: [String] type
    // If you are sure you will never need to use role groups, then
    // you can specify [String] as the type
    roles: {
        type: [String],
        optional: true,
        label: "Pick Roles",
        autoform: {
            type: 'Select',
            omit: true,
            options: function () {
                var roles = Roles.getAllRoles().fetch();

                return _.map(roles, function (c, i) {
                    return {label: c.name, value: c.name};
                });
            }
        }
    }
});

// Attach the user schemas to Meteor.users
Meteor.users.attachSchema(Schemas.User);
