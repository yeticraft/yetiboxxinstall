#!/usr/bin/env bash

# Downloads from our pages
# https://bitbucket.org/Growlf/yeticraftmc/wiki/Plugin%20download%20pages

wget -q -nc http://dev.bukkit.org/media/files/906/898/WorldBorder.jar
wget -q -nc http://dev.bukkit.org/media/files/881/691/worldguard-6.1.jar
wget -q -nc http://dev.bukkit.org/media/files/880/435/worldedit-bukkit-6.1.jar
wget -q -nc http://dev.bukkit.org/media/files/894/359/Vault.jar
wget -q -nc http://scriptcraftjs.org/download/latest/scriptcraft-3.1.11/scriptcraft.jar
wget -q -nc http://dev.bukkit.org/media/files/856/566/multiworld.jar
wget -q -nc https://ci.drtshock.net/job/MCMMO/lastSuccessfulBuild/artifact/target/mcMMO.jar
wget -q -nc http://ci.griefcraft.com/job/LWC/lastSuccessfulBuild/artifact/core/target/LWC.jar
wget -q -nc http://ci.md-5.net/job/CraftBook/lastSuccessfulBuild/artifact/target/craftbook-3.9-SNAPSHOT.jar
wget -q -nc http://dev.bukkit.org/media/files/880/435/worldedit-bukkit-6.1.jar
wget -q -nc http://dev.bukkit.org/media/files/798/498/commandbook-2.4.zip
unzip -o commandbook-2.4.zip


# wget -q -nc http://ci.griefcraft.com/job/LWC/lastSuccessfulBuild/artifact/modules/economy/target/LWC-Economy.jar
# wget -q -nc http://ci.citizensnpcs.co/view/All/job/Denizen/lastSuccessfulBuild/artifact/target/denizen-0.9.7-SNAPSHOT.jar
# wget -q -nc http://dev.bukkit.org/media/files/901/556/Citizens.jar
# wget -q -nc http://ci.citizensnpcs.co/view/All/job/Depenizen/lastSuccessfulBuild/artifact/target/Depenizen.jar
# wget -q -nc https://www.spigotmc.org/resources/bungeeperms.25/download?version=16536
