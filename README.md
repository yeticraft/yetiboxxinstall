# THIS CODE IS IN INITIAL STAGES - NOT RECOMMENDED FOR END-USERS ...YET!

More details and information coming soon!  Give us a few more days, unless you are a fearless adventurer. :)
In the meantime, if you are a bleeding edge kind of person - forge on and let us know it goes!!!  
Use the [issues tracker](https://bitbucket.org/Growlf/yetiboxxinstall/issues) to let us know about anything
that fails, or to offer suggestions - or even to request to join our project.

You can chat with us on our [Discord Server](https://discord.gg/nq8BC8C) and find out about upcoming events/features, and discuss all nature of things.

# Welcome to the new YetiBoxX installer code base.  


# Roadmap of features and processes

## What this does (will do)

* Gather basic user and system information/config choices with prompts and help text
* Ensure that the correct java is installed
* check and offer to install OS updates
* Offer several options in a menu:
    * Select a server type
        * _Spigot_ (initially this project will only be for Spigot)
        * MCPE
        * Vanilla
    * Download and/or build the selected server type, then install it
    * Configure the selected server type
        * server.properties
        * bukkit.yml
        * spigot.yml
    * Install/configure plugin "packs" (preselected server plugin sets and configurations)
    * Set auto-run server using init.d (only one can be selected - the current type)
    * Check and set the EULA agreement file for Spigot
    * Connect the box to YetiCraft.net (with account_key from YetiCraft)
    * Backup/restore settings, worlds etc to local or git repository

##Things to Remember as we design and build this application prototype

* This script should be able to run on any Debian box when finished
* This project is a prototype for the actual Meteor based UI that is also being built and which will offer a more user friendly interface for beginners.
* This project is only the "box" side of the final tool - so no need for network scanning tools or other target determination is needed in it - only things that help set up the box that this script is running on


------------------------------------------------------------------------------

# Installing from scratch on any Debian based system

## Quick and dirty install:

1. **BASE OS** - Install a debian OS (VirtualBox, Raspbian on a Raspberry Pi, or what have you)
    * If you are using a VM, it is recommended to use "briged mode" for the network interface
    * For a Pi with Raspbian - use Adafruit's tool, for now, from https://learn.adafruit.com/the-adafruit-raspberry-pi-finder/overview
1. **PACKAGES and YetiBoxX Software** - After you have a running Pi with Raspbian (or similar) running: open a terminal to your Pi and type (or paste into a terminal window such as the one available in the PiFinder from Adafruit)

```
#!shell

curl https://bitbucket.org/yeticraft/yetiboxxinstall/raw/master/system/bootstrap.sh | bash
```

## Optional steps
1. **EASIER ADMIN** - Add yourself to the 'sudo' group like so: ```adduser pi sudo```
    1. remove the password requirement for the "pi" user by editing the ```/etc/sudoers``` file with nano: change the line that refers to allowing "all members of the group sudo to execute any command" to be the following:  ```%sudo	ALL=(ALL) NOPASSWD:ALL```
1. **COLOR PROMPT** - Using nano, edit the ```~pi/.bashrc```  file, uncomment the line that says ```force_color_prompt=yes``` - then logout as root and back in as user "pi", you should now see the prompt in a colorful new way.
1. **HOSTNAME** - Edit the ```/etc/hostname``` file to reflect a more personal identification of the YetiBoxX.  Just remember that the hostname should only be a single "word" with no punctuation or anything beyond simple alpha-numerics.

## Developer steps
1. **SECURE REMOTE ACCESS** - Once logged in as "pi", create some keys for the box like so:  ```ssh-keygen```  ...just hit enter and take all the defaults.  
1. **REPO** - Add the pub key to your bitbucket account so that this account can access with read-write to the repo, an re-clone the repository using your own ID instead of the generic one in the general instructions.


...and that is about it!  Now you can cd into the ~/YetiBoxX/yetiboxui folder and execute ```meteor run```.  Brows to the IP of the system with any browser on port 3000.

------------------------------------------------------------------------------

# Notes to self

## User account and Password

On a RaspberryPi, the default username and password for a Raspbian install is: ```pi``` and ```rasberry```

## Accessing the YetiBoxX fileststem

### SSHFS
You can use sshfs or winsshfs to mount the YetiBoxX filesystem like so:

```sshfs pi@<REMOTE IP>:/home/pi/YetiBoxX ~/Desktop/yetiboxxinstall```


### Windows/SAMBA File share

From a shell on the RaspberryPi, execute ```sudo smbpasswd -a pi``` and follow the directions.  This will enable the user `pi` to view and edit files over Windows Fileshareing.  Now add a share by editing ```/etc/samba/smb.conf```
```
[pihome]
   comment= Pi Home
   path=/home/pi
   browseable=Yes
   writeable=Yes
   only guest=no
   create mask=0777
   directory mask=0777
   public=no
```

## Meteor ENV 

To run the Meteor "bundle" (generated from the build of the GUI), you will need to set the following: 

``` 
#!shell

export MONGO_URL='mongodb://localhost'
export ROOT_URL='http://localhost'
export PORT=3000
```

This will be wrapped up into a script soon!