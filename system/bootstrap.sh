#!/usr/bin/env bash

# Clone the repository
git clone https://bitbucket.org/yeticraft/yetiboxxinstall.git ~/YetiBoxX

# Add the system modules and settings that we need
sudo bash ~/YetiBoxX/system/INSTALL